#!/bin/sh

ROOT=`pwd`

for pj in `find . -name package.json ! -path **/node_modules/**`
do
#  echo $pj
  p=`dirname $pj`
  echo $p
  cd $p
  npm run clean-build
  cd $ROOT
  echo $p done
done

echo done with all
