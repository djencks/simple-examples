= {description}
:description: indexDescriptionList Examples

== Page Coordinates


page-component-name: component

page-component-version: 1.0

page-module: ROOT

page-relative-src-path: {page-relative-src-path}

== Examples of `indexDescriptionList` block macro

All `indexDescriptionList` uses specify `style=horizontal`.
Whether it has an effect depends on whether the use creates a new dlist or appends to an existing one.

First, a plain description list:

[source,adoc]
indexDescriptionList::[descriptionformat=description,style=horizontal]

indexDescriptionList::[descriptionformat=description,style=horizontal]

Next we expect a list:

[source,adoc]
----
This is a dlist item:: First!

indexDescriptionList::[level=1,descriptionformat=description,style=horizontal]
----

This is a dlist item:: First!
Next is level 1

indexDescriptionList::[level=1,descriptionformat=description,style=horizontal]

This is a dlist item:: Second!
This is a sub item::: Sub1!
Third level A:::: SubSub1!
Next is level 1

indexDescriptionList::[level=1,descriptionformat=description,style=horizontal]

This is a dlist item:: Third!
This is a sub item::: Sub1!
Third level B:::: SubSub1!
Next is level 2

indexDescriptionList::[level=2,descriptionformat=description,style=horizontal]

This is a dlist item:: Fourth!
This is a sub item::: Sub1!
Third level C:::: SubSub1!
Next is level 3

indexDescriptionList::[level=3,descriptionformat=description,style=horizontal]

This is a dlist item:: Fifth!
This is a sub item::: Sub1!
Third level D:::: SubSub1!
Next is level 4 (which doesn't work well using .adoc source)

indexDescriptionList::[level=4,descriptionformat=description,style=horizontal]

this is not a list item

next should be:: separate (no level)

indexDescriptionList::[module=module1,descriptionformat=doctitle,style=horizontal]

next has relative filter (no level)

indexDescriptionList::[component=*,version=*,module=*,relative=topic*/*.adoc,descriptionformat=description,style=horizontal]

== An example without links

[horizontal]
Manual list::
  This is some hardcoded content

indexDescriptionList::[module=module1,termAttribute=mod2,descriptionformat=doctitle,style=horizontal]
