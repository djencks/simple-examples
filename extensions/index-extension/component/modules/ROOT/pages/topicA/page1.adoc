= Title component 1.0 ROOT topicA/page1.adoc
:description: Page A1
:page-name: page1
:odd: true

== Page Coordinates

page-component-name: component

page-component-version: 1.0

page-module: ROOT

page-relative: topicA/page1.adoc

description: {description}

== A page

And some content.
