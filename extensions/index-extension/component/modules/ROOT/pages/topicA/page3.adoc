= Title component 1.0 ROOT topicA/page3.adoc
:description: Page A3
:page-name: page3
:odd: true

== Page Coordinates

page-component-name: component

page-component-version: 1.0

page-module: ROOT

page-relative: topicA/page3.adoc

description: {description}

== A page

And some content.
