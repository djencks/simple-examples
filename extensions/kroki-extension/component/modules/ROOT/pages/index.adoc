= {page-component-title}

== Ways of using link:https://github.com/Mogztter/asciidoctor-kroki[asciidoctor-kroki]

For this example, you must install asciidoctor-kroki so it is accessible to your Antora installation.
If you have installed Antora globally, you can run:

```
npm install -g asciidoctor-kroki
```

See the package.json for an alternate more self contained installation method.

Configure asciidoctor to use this extension in your playbook:

[source,yml]
----
asciidoc:
  extensions:
    - asciidoctor-kroki
  attributes:
    allow-uri-read: true
----

If you are not using `inline` or `interactive` svg images, you probably want to also add the attribute `kroki-fetch-diagram@: true`.
This will cause asciidoctor-kroki to download the diagram images from the kroki server and install them in the Antora images directory.
As a result, your generated site will not need to access a kroki server.
The '@' allows the attribute to be reset in page header or body attributes.

For inline and interactive svg images, this has the, most likely undesired, effect of duplicating the image in the images directory and the page text.
Furthermore, at the moment, Antora cannot produce inline svg from images in the content catalog.

== Notes on content

The html pages in a browser will look rather repetitive.
It is perhaps more interesting to look at the html source to see the effects of the different configurations.

Currently this example only explores plantuml source format diagrams.
With the exception of `ditaa` source, for which only `png` output is supported, other diagram types should have similar capabilities and configuration.

=== xref:sourcelocation.adoc[Choices for source diagram location]

This page demonstrates the possible locations for the diagram source using a `[plantuml]` block.

=== xref:embedding.adoc[Choices for generated html]

This page demonstrates some of the choices for the style of generated html. Since `data-uri` is not officially supported by Antora, and currently does not work for `png` impages pending an asciidoctor upgrade, there are no `data-uri` examples.

=== xref:embeddingblockmacro.adoc[Choices for generated html, using the block macro]

This demonstrates the same choices but using the `plantuml::` block macro with a reference to the diagram source in `partials`



