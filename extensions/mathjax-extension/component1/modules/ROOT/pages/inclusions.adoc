= Including pages with stem content

This includes two other pages, with no stem configuration on this page.

include::asciimath.adoc[leveloffset=+1]

include::latexmath.adoc[leveloffset=+1]