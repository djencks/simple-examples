= {page-component-title}
:page-relative-src-path: {page-relative}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}

source-highlighter: {source-highlighter}

== A page

And some content.

[source,js]
----
module.exports.register = function (registry, config) {
  config.length || (config.theme = DEFAULT_THEME)
  registry.on('contentClassified', async ({ siteAsciiDocConfig }) => {
    highlighter = await shiki.getHighlighter(config)
    const register = { register: asciidoctorRegister }
    siteAsciiDocConfig.extensions ? siteAsciiDocConfig.extensions.push(register) : siteAsciiDocConfig.extensions = [register]
  })
}
----

[source]
----
module.exports.register = function (registry, config) {
  config.length || (config.theme = DEFAULT_THEME)
  registry.on('contentClassified', async ({ siteAsciiDocConfig }) => {
    highlighter = await shiki.getHighlighter(config)
    const register = { register: asciidoctorRegister }
    siteAsciiDocConfig.extensions ? siteAsciiDocConfig.extensions.push(register) : siteAsciiDocConfig.extensions = [register]
  })
}
----

[source,nosuchlanguage]
----
module.exports.register = function (registry, config) {
  config.length || (config.theme = DEFAULT_THEME)
  registry.on('contentClassified', async ({ siteAsciiDocConfig }) => {
    highlighter = await shiki.getHighlighter(config)
    const register = { register: asciidoctorRegister }
    siteAsciiDocConfig.extensions ? siteAsciiDocConfig.extensions.push(register) : siteAsciiDocConfig.extensions = [register]
  })
}
----
